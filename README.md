# Gitlab CLI

This CLI gives the user easy access to the gitlab repository.
Using this CLI one can view, update, create and delete variables related to both project and group levels. With basic commands one can easily know the last jobs with their duration for a specific period, a sorted list of users, a list of files in different projects. This CLI minimizes the effort of setting a variable across various projects manually. The modular directory structure of this project allows anyone to add new functionalities without any modification to the driver classes.

## Getting Started

Available on cc-pj-mbb-tooling / gitlab-cli for now

### Installing

Prerequisite for running this CLI is having an npm install. After that its good to go. 

## Running the tests

npm test

A few example unit tests.

## Usage

The documentation below will guide the user towards using the various features of the CLI. 

The modes folder contains the various types of operations allowed. The vars sub folder has classes for performing operations related to project and group level variables. 

```
Use ./index.js help for a comprehensive command list

1.To update a group level variable:

//Here the g refers to group level. Replace it with p for project level. The following two
arguments refer to the key of the variable and the new value of the variable.

./index.js updatevars g foo go  

This is the structure of the commands for all the functions related to variables. 

2.To see the jobs performed in the last 10 days(any number)
 ./index.js lastjobs 10

3.To see all jobs (use carefully, might take long time)
./index.js dumpjobs

4.To see all files 
./index.js dumpfiles

5.To view a sorted list of users (for example sorted according to id)
./index.js listuser id 

```
Apart from this, ./index.js projects and ./index.js groups will give the entire project and group details as json which might be of some use to the developer during debugging. 


### coding style tests

JavaScript Standard Style has been used.


## Authors

Gerrit Schmitz - Initial work
Subham Banerjee - Added features
Acknowledgments - Sinan Ayhan


 


