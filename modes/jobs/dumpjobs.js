/**
 * This is the lastjobs that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class LastJobs
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */
const Project = require('gitlab-api').Project
let last
module.exports = (args, config, gitlab) => {
  (async () => {
    if (args[0]) {
      let days = args[0] // jobs in the last args[0] days
      let date = new Date()
      last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000))
    }

    for (const project of await Project.all(gitlab, config)) {
      for (const job of await project.jobs) {
        if (job && last) {
          if (job.started_at > last) {
            console.log(project.name_with_namespace, '#', job.name, '#', job.started_at.toISOString(), '#', job.duration)
          }
        } else if (job) {
          console.log(project.name_with_namespace, '#', job.name, '#', job.started_at.toISOString(), '#', job.duration)
        } else {
          console.log(project.name_with_namespace, '# job not found')
        }
      }
    }
    console.log('\n' + 'Use ./gitlab-cli/index.js help for further information')
  })().catch(console.error)
}
