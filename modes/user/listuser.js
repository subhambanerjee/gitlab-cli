/**
 * This is the listuser class that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class ListUser
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */

const Project = require('gitlab-api').Project

/** sort function for listuser - sorts list by any field */
let sortBy = (function () {
  const _defaults = {
    parser: (x) => x,

    desc: false // make desc : true for descending order
  }

  const isObject = (o) => o !== null && typeof o === 'object'
  const isDefined = (v) => typeof v !== 'undefined'

  function getItem (x) {
    const isProp = isObject(x) && isDefined(x[this.prop])
    return this.parser(isProp ? x[this.prop] : x)
  }

  return function (array, options) {
    if (!(array instanceof Array) || !array.length) { return [] }
    const opt = Object.assign({}, _defaults, options)
    opt.desc = opt.desc ? -1 : 1
    return array.sort(function (a, b) {
      a = getItem.call(opt, a)
      b = getItem.call(opt, b)
      return opt.desc * (a < b ? -1 : +(a > b))
    })
  }
}())

module.exports = (args, config, gitlab) => {
  (async () => {
    if (args[0]) {
      for (const project of await Project.all(gitlab, config)) {
        const users = await project.users
        sortBy(users, {prop: args[0]})
      // check if users is empty or not
        if (Array.isArray(users)) {
          for (const user of users) {
            console.log(project.name_with_namespace, '#', user.username, '#', user.name, '#', user.id, '#', user.state)
          }
        }
        console.log()
      }console.log('\n' + 'Use ./gitlab-cli/index.js help for further information')
    } else {
      console.log('\n' + 'Specify the field according to which you want the sort:' + '\n' + './index.js listuser id for sorting users according to their id' + '\n')
    }
  })().catch(console.error)
}
