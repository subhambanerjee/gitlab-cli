/**
 * This is the createvars class that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class CreateVars
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */

const Project = require('gitlab-api').Project

const Group = require('gitlab-api').Group

module.exports = (args, config, gitlab) => {
  (async () => {
    const level = args[0]
    let type
    if (level === 'p') {
      type = Project
    } else if (level === 'g') {
      type = Group
    }
    if (type === Group || type === Project) {
      for (const item of await type.all(gitlab, config)) { // item is either project or group
        const variable = await item.variable(args[1]).get()
        let itemName = level === 'p' ? item.name_with_namespace : item.name

        if (variable.key) {
          console.log(itemName, '### already has variable with this key. Please use updatevars to update it')
        } else {
          let temp = await item.variable(args[1]).create(args[2])
          if (temp.key) {
            console.log(itemName, '### variable has been created')
          } else {
            console.log(itemName, '### Not allowed to create')
          }
        }
      }
    } else {
      console.log(`
      First argument must specify type p for project or g for group
      For example: To create a project level variable foo with value go use the following
      ./gitlab-cli/index.js createvars p foo go
      Use ./gitlab-cli/index.js help for further information`)
    }
  })().catch(console.error)
}
