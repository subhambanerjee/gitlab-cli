/**
 * This is the removevars class that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/group},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class RemoveVars
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */
const Project = require('gitlab-api').Project

const Group = require('gitlab-api').Group

module.exports = (args, config, gitlab) => {
  (async () => {
    const level = args[0]
    let type
    if (level === 'p') {
      type = Project
    } else if (level === 'g') {
      type = Group
    }
    if (type === Group || type === Project) {
      for (const item of await type.all(gitlab, config)) { // item is either project or group
        const variable = await item.variable(args[1]).get()
        let itemName = level === 'p' ? item.name_with_namespace : item.name

        if (variable.key) {
          item.variable(args[1]).delete().catch(() => {})
          console.log(itemName, '###', variable.key, 'has been deleted')
        } else {
          console.log(itemName, '### does not have variable with key', args[1])
        }
      }
    } else {
      console.log(`
      First argument must specify type p for project or g for group
      For example: To remove a project level variable foo use the following:
      ./gitlab-cli/index.js removevars p foo
      Use ./gitlab-cli/index.js help for further information`)
    }
  })().catch(console.error)
}
