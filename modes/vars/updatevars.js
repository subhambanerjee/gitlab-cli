/**
 * This is the updatevars class that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/group},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class UpdateVars
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/group
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */
const Project = require('gitlab-api').Project

const Group = require('gitlab-api').Group

module.exports = (args, config, gitlab) => {
  (async () => {
    const level = args[0]
    let type
    if (level === 'p') {
      type = Project
    } else if (level === 'g') {
      type = Group
    }
    if (type === Group || type === Project) {
      for (const item of await type.all(gitlab, config)) {
        const variable = await item.variable(args[1]).get()
        let itemName = level === 'p' ? item.name_with_namespace : item.name

        if (variable.key) {
          if (variable.value === args[2]) {
            console.log(itemName, '### Key', variable.key, 'already had value', args[2])
          } else {
            let temp = await item.variable(args[1]).update(args[2])
            console.log(itemName, '###', variable.key, 'has been updated from', variable.value, 'to', temp.value)
          }
        } else {
          console.log(itemName, '### does not have variable with key', args[1])
        }
      }
    } else {
      console.log(`
      First argument must specify type p for project or g for group
      For example: To update a project level variable foo with value go use the following:
      ./gitlab-cli/index.js updatevars p foo go
      Use ./gitlab-cli/index.js help for further information`)
    }
  })().catch(console.error)
}
