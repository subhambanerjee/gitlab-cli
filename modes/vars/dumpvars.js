/**
 * This is the dumpvars class that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/group},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class DumpVars
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/group
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */

const Project = require('gitlab-api').Project
const Group = require('gitlab-api').Group

module.exports = (args, config, gitlab) => {
  (async () => {
    const level = args[0] // project or group level
    let type
    if (level === 'p') {
      type = Project
    } else if (level === 'g') {
      type = Group
    }
    if (type === Group || type === Project) {
      for (const item of await type.all(gitlab, config)) { // item is either project or group
        const variables = await item.variables
      // to check if variables is empty or not
        if (Array.isArray(variables)) {
          for (const variable of variables) {
            if (level === 'p') {
              console.log(item.name_with_namespace, '#', variable.key, '=', variable.value)
            } else {
              console.log(item.name, '#', variable.key, '=', variable.value)
            }
          }
        }
      }
    } else {
      console.log(`
      First argument must specify type p for project or g for group
      For example: To dump project level variables use the following:
      ./gitlab-cli/index.js dumpvars p
      Use ./gitlab-cli/index.js help for further information`)
    }
  })().catch(console.error)
}
