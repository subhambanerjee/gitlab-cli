/**
 * This is the dumpfiles that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class DumpFiles
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */
const Project = require('gitlab-api').Project

module.exports = (args, config, gitlab) => {
  (async () => {
    for (const project of await Project.all(gitlab, config)) {
      const pipe = await project.file(args[0] || '.gitlab-ci.yml', args[1] || 'master')

      if (pipe) {
        console.log(project.name_with_namespace, '#', pipe.toString().replace(/(\n)+/g, `\n${project.name_with_namespace} # `))
      }
    }
    console.log('\n' + 'Use ./gitlab-cli/index.js help for further information')
  })().catch(console.error)
}
