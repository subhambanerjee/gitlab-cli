module.exports = (args, config, gitlab) => {
  (async () => {
    console.log(`
                           Welcome to the Gitlab CLI

         Description                                                 Command

    1.  see project level variables                         ./gitlab-cli/index.js vars dump p
    2.  see group level variables                           ./gitlab-cli/index.js vars dump g
    3.  create project level variable                       ./gitlab-cli/index.js vars create p [new variable] [new value]
    4.  create group level variable                         ./gitlab-cli/index.js vars create g [new variable] [new value]
    5.  update project level variable                       ./gitlab-cli/index.js vars update p [variable key] [new value]
    6.  update group level variable                         ./gitlab-cli/index.js vars update  g [variable key] [new value]
    7.  remove project level variable                       ./gitlab-cli/index.js vars remove p [variable key]
    8.  remove group level variable                         ./gitlab-cli/index.js vars remove g [variable key]
    9.  see sorted user list (ex: sort by id)               ./gitlab-cli/index.js user id
    10. see list of projects                                ./gitlab-cli/index.js proj
    11. see jobs in last n days                             ./gitlab-cli/index.js jobs n(Here n is any integer)
    12. see all jobs(previous command is preferred more)    ./gitlab-cli/index.js jobs
    13. see all files                                       ./gitlab-cli/index.js files
    
    14. run unit tests                                       npm test

    User list can be sorted by any field visible to the user. The field last_activity_on might be useful
    for the admin. 
        
    

    `)
  })().catch(console.error)
}
