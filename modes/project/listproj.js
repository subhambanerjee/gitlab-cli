/**
 * This is the listproj class that requires the module {@link module : gitlab-cli/project},
 * {@link module : gitlab-cli/config} and
 * {@link module: gitlab-cli/gitlab}
 * @class ListProj
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/gitlab
 */
const Project = require('gitlab-api').Project

module.exports = (args, config, gitlab) => {
  (async () => {
    let count = 0
    for (const project of await Project.all(gitlab, config)) {
      console.log(project.name_with_namespace, '#', project.id)
      count++
    }
    console.log()
    console.log('There are ', count, ' projects' + '\n' + 'Use ./index.js help for further information')
  })().catch(console.error)
}
