'use strict'

/**
 * This is the Test class for the CLI that requires the module {@link module : gitlab-cli/project} and
 * {@link module : gitlab-cli/node_modules/chai}
 * @class Test
 * @requires module:gitlab-cli/project
 * @requires module:gitlab-cli/node_modules/chai
 */

const chai = require('chai')
// using expect assertion method of chai
const expect = chai.expect
const Project = require('gitlab-api').Project
const config = require('../config.json')

/**
 *mocking Gitlab
 */
const gitlabMock = {
  get: () => Promise.resolve({value: 'cs', name: 'gitlab-cli', length: 6}),
  put: () => Promise.resolve({value: 'wire'}),
  del: () => {},
  post: () => {},
  object2params: () => []
}

/**
 *Testing project class
 */
describe('Projects', function () {
  it('variable method returns a promise after get, update, delete or create', function (done) {
    const project = new Project(gitlabMock, config)
    const variable = project.variable('barb').update(`wire`)

    variable.then((variable) => {
      expect(variable.value).to.equal('wire')
      done()
    })
  })
  it('gets users, testing length of users array', function (done) {
    const project = new Project(gitlabMock, config)
    const user = project.users

    user.then((user) => {
      expect(user.length).to.be.above(5)
      done()
    })
  })
  it('test users property in project class', function (done) {
    const project = new Project(gitlabMock, config)
    const user = project.users
    user.then((user) => {
      expect(user).to.have.lengthOf(6)
      done()
    })
  })
})

/**
 * Testing gitlab class
 */
describe('Gitlab', function () {
  it('gives a string array when object is passed', function () {
    const objstr = gitlabMock.object2params({})
    expect(objstr).to.be.an.instanceof(Array).that.is.empty
  })
  it('empty string when no object passed', function () {
    const objstr = gitlabMock.object2params({})
    expect(objstr).to.deep.equal([])
  })
})

/**
 * Testing Variable class
 */
describe('Variable', function () {
  it('getter for variable', function (done) {
    const project = new Project(gitlabMock, config)
    const variable = project.variable('barb').get()

    variable.then((variable) => {
      expect(variable.value).to.equal('cs')
      done()
    })
  })
})
