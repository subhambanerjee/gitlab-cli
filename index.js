#!/usr/bin/env node
'use strict'

const argsprefix = 2
const args = process.argv.slice(argsprefix)
const mode = args.shift() // gets the mode such as vars, files

/**
 * This is the index class that requires the module {@link module : gitlab-cli/config} and
 * {@link module : gitlab-cli/node_modules/bunyan}
 * @class Index
 * @requires module:gitlab-cli/config
 * @requires module:gitlab-cli/node_modules/bunyan
 */

const Logger = require('bunyan')
const Gitlab = require('gitlab-api').Gitlab

const config = require('./config.json')
const gitlab = new Gitlab(config.url, config.token)

Object.assign(process.env, config.env)

// applogger for recording tasks performed
let applogger = new Logger(config.logger)
let main

// searches for js files in modes folder
try {
  switch (mode) {
    case 'vars':
      let classname
      const type = args.shift() // the function to be done
      if (type === 'dump') {
        classname = 'dumpvars'
      } else if (type === 'update') {
        classname = 'updatevars'
      } else if (type === 'create') {
        classname = 'createvars'
      } else if (type === 'remove') { classname = 'removevars' }
      main = require(__dirname.concat(`/modes/${mode}/${classname}.js`))
      break
    case 'jobs':

      main = require(__dirname.concat(`/modes/jobs/dumpjobs.js`))

      break
    case 'files': main = require(__dirname.concat(`/modes/files/dumpfiles.js`)); break
    case 'user': main = require(__dirname.concat(`/modes/user/listuser.js`)); break
    case 'proj': main = require(__dirname.concat(`/modes/project/listproj.js`)); break
    default: main = require(__dirname.concat(`/modes/help.js`))
  }
} catch (err) {
  console.log('use ./gitlab-cli/index.js help' + '\nError: ' + err)  // opens help if no operation is specified
}

if (main) {
  applogger.info(`${mode}`)

  main(args, config, gitlab)
}
